# Crab Grab
A blazingly basic neofetch clone in rust.
Would have been called crabfetch, however that was already taken.

![image info](crabgrab.png)

[![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)