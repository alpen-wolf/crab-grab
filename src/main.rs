use colored::*;
use sysinfo::*;
use whoami::*;

fn main() {
    let mut data = Fetch::default();
    data.fetch();
    data.display();
}

type OpStr = Option<String>;

#[derive(Debug)]
struct Fetch {
    username: OpStr,
    hostname: OpStr,
    os: OpStr,
    kernel: OpStr,
    cpu: OpStr,
    memory: (OpStr, OpStr),
}

impl Default for Fetch {
    fn default() -> Self {
        let unknown = Some(String::from("Unknown"));
        Self {
            username: unknown.clone(),
            hostname: unknown.clone(),
            os: unknown.clone(),
            kernel: unknown.clone(),
            cpu: unknown.clone(),
            memory: (unknown.clone(), unknown.clone()),
        }
    }
}

impl Fetch {
    fn fetch(self: &mut Self) {
        let mut sys = System::new_all();
        sys.refresh_all();
        self.username = Some(username());
        self.hostname = sys.host_name();
        self.kernel = sys.kernel_version();
        self.os = sys.long_os_version();
        self.cpu = Some(sys.global_cpu_info().brand().to_string());
        self.memory = (
            Some((((sys.used_memory() / 1048576) as f64) / 1000.).to_string()),
            Some((((sys.total_memory() / 1048576) as f64) / 1000.).to_string()),
        );
    }

    fn display(self: Self) {
        println!(
            "  {start}
    {username}@{hostname}
    {os·}{os},
    {kernel·}{kernel},
    {cpu·}{cpu},
    {mem·}{mem1} {gib} / {mem2} {gib},
  {end}",
            start = "╔═══════════════════════════════════════════════════════════════════════════════════╗"
                .magenta(),
            username = self.username.unwrap_or_default().red(),
            hostname = self.hostname.unwrap_or_default().red(),
            os· = "os: ".red(),
            os = self.os.unwrap_or_default().green(),
            kernel· = "kernel: ".red(),
            kernel = self.kernel.unwrap_or_default().green(),
            cpu· = "cpu: ".red(),
            cpu = self.cpu.unwrap_or_default().green(),
            mem· = "mem: ".red(),
            mem1 = self.memory.0.unwrap_or_default().green(),
            mem2 = self.memory.1.unwrap_or_default().green(),
            gib = "GiB".green(),
            end= "╚═══════════════════════════════════════════════════════════════════════════════════╝".magenta(),
        )
    }
}
